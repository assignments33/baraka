# Project

Test assignment for the role of technical lead at Baraka company.

Project requires:
* Apache Maven 3.8.5 
* Java version: 17.0.2

# Build & run

Run the command below to build the project:

```sh
  mvn clean package
```

To get coverage run:

```sh
   mvn clean compile jacoco:prepare-agent test jacoco:report
```

Report can be found in `target/site/jacoco/index.html`.

Built executable will be located in `target/application.jar`.

To run the application do the following command being in project root folder:

```sh
  java -jar target/application.jar
```

By default Swagger page is available at [http://localhost:8080/swagger-ui.html](http://localhost:8080/swagger-ui.html):

* `/api/stock` - get a list of available stock symbols
* `/stock/{name}/prices` - get a list of min/max prices of an asked stock

# Configuration

Default configuration is located in [resources](src/main/resource) folder. The following user parameters are available:

```yaml
application:
  market-data:
    error-limit-to-reconnect: 3  # limits the number of attemps to connect, 0 - no limit
    prices-api: 'ws://...'       # market-data websocket URI
  pipeline:
    frame-duration: PT1M         # interval duration
    frame-shift: PT0S            # time-scale shift
    frame-history: 15            # number of intervals to keep
    stock-filter: ['ABT', 'AMD'] # a list of symbols to pass through a filter, empty list turns off filtering 
```

# Task description

Build a service ("Min Max Service") that returns the min and max prices for a particular stock, with a granularity of a minute.

## Input

The stream of prices of approx 1500 stocks (AAPL, MSFT, etc) is available at `ws://b-mocks.dev.app.getbaraka.com:9989` (WebSocket, no authentication).

The format of the tick is :

```json
  {
    "data": [
      {
        "p": 93.85099174403666, # stock price
        "s": "AAPL",            # stock/symbol
        "t": 1653577493139,     # epoch milliseconds
        "v": 10
      }
    ],
    "type": "trade"
  }
```

## Requirements

Stream processing:

* Minimums and maximums should be determined on the price field.
* Minimums and maximums should be calculated in the event of a new minute start. 
* You can determine what minute a particular tick belongs to from the time field. FYI, time is in epoch millis(UTC).
* If you catch the stream in the middle of a minute then you should calculate the minimum and maximum for that minute as well.

API:

* API should respond with a list of Min/Max prices of an asked stock(including the current minute). E.g. if we call the API after 5 minutes of your service it should return the 5 (built) + 1 (in progress) = 6 total entries.
* The request should contain the stock name(e.g. AAPL)
* The response should contain a list of objects(one per minute) with the following attributes : 
    * Start time of an interval
    * End time of an interval
    * The minimum price observed at this time interval 
    * The maximum price observed at this time interval 

## NFR

Please create a service in a way that it can be easily modified to support bigger/smaller time intervals.

Data can be stored in memory or any other choice of your cache/database.

Stocks and symbols are the same things.

You'll be graded based on respect to the requirements, code quality, test coverage, and documentation. You can use Java(spring-boot) or nodeJs to complete the assignment.

Please share the GitHub repository link that contains the code along with a docker image that can simply run on our machines along with its accessibility  explained in README.md

# End