package org.javamaker.assignment.baraka.model;

import java.util.List;

import lombok.AccessLevel;
import lombok.Data;
import lombok.experimental.FieldDefaults;

/**
 * Stream record as we receive it from WebSocket API.
 * <p>
 * Record sample:
 *
 * <pre>
 * {
 *    "data": [
 *        {
 *            "p": 88.9004008471895,
 *            "s": "TSLA",
 *            "t": 1653398801574,
 *            "v": 10
 *        }
 *    ],
 *    "type": "trade"
 * }
 * </pre>
 */
@Data
@FieldDefaults(level = AccessLevel.PRIVATE)
public class MarketMessage {
  /** Price data array. */
  List<StockPrice> data;
  /** Event type. */
  String type;
}
