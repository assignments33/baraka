package org.javamaker.assignment.baraka.model;

import java.math.BigDecimal;
import java.time.Instant;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Builder;
import lombok.Value;

@Schema(description = "Min/max range for given symbol and interval")
@Builder
@Value
public class StockPriceRange {
  @Schema(description = "Start time of an interval")
  Instant startTime;
  @Schema(description = "End time of an interval")
  Instant endTime;
  @Schema(description = "The minimum price observed at this time interval")
  BigDecimal minPrice;
  @Schema(description = "The maximum price observed at this time interval")
  BigDecimal maxPrice;
}
