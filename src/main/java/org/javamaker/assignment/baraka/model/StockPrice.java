package org.javamaker.assignment.baraka.model;

import java.math.BigDecimal;
import java.time.Instant;

import com.fasterxml.jackson.annotation.JsonAlias;

import lombok.AccessLevel;
import lombok.Data;
import lombok.experimental.FieldDefaults;

/**
 * Single market price record as we get it from {@link MarketMessage#getData()}.
 * <p>
 * Record sample:
 *
 * <pre>
 * {
 *   "p": 88.9004008471895,
 *   "s": "TSLA",
 *   "t": 1653398801574,
 *   "v": 10
 * }
 * </pre>
 */
@Data
@FieldDefaults(level = AccessLevel.PRIVATE)
public class StockPrice {
  /** Stock price. */
  @JsonAlias({ "Price", "p", "P" })
  BigDecimal price;
  /** Stock name. */
  @JsonAlias({ "Symbol", "symbol", "Stock", "s" })
  String stock;
  /** Epoch milliseconds (UTC). */
  @JsonAlias({ "Time", "t" })
  Instant time;
  /** trade volume. */
  @JsonAlias({ "Volume", "v" })
  Integer volume;
}
