package org.javamaker.assignment.baraka;

import lombok.experimental.UtilityClass;

@UtilityClass
public class Constants {
  public final String QUALIFIER_REST = "rest";
  public final String QUALIFIER_WEBSOCKET = "websocket";

  public final String TRACE_EVENT_ENDPOINT_CONNECTED = "endpoint.connected";
  public final String TRACE_EVENT_ENDPOINT_DISCONNECTED = "endpoint.disconnected";
  public final String TRACE_EVENT_ENDPOINT_EXCEPTION = "endpoint.exception";
  public final String TRACE_EVENT_MESSAGE_RECEIVED = "message.received";
  public final String TRACE_EVENT_MESSAGE_PROCESSED = "message.processed";
  public final String TRACE_EVENT_MESSAGE_FAILURE = "message.failure";

  public final String TRACE_TAG_STATUS_REASON = "status.reason";
  public final String TRACE_TAG_STATUS_CODE = "status.code";
  public final String TRACE_TAG_EXCEPTION = "exception";
}
