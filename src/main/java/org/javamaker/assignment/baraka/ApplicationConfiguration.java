package org.javamaker.assignment.baraka;

import org.javamaker.assignment.baraka.ApplicationProperties.MarketDataProperties;
import org.javamaker.assignment.baraka.ApplicationProperties.OpenApiProperties;
import org.javamaker.assignment.baraka.ApplicationProperties.PipelineProperties;
import org.javamaker.assignment.baraka.model.MarketMessage;
import org.javamaker.assignment.baraka.services.EventDecoder;
import org.javamaker.assignment.baraka.services.EventEndpoint;
import org.javamaker.assignment.baraka.services.StockPriceService;
import org.javamaker.assignment.baraka.util.ObjectMappers;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.cloud.sleuth.Tracer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.socket.client.WebSocketClient;
import org.springframework.web.socket.client.standard.StandardWebSocketClient;

import com.fasterxml.jackson.databind.ObjectMapper;

import io.swagger.v3.oas.models.OpenAPI;
import io.swagger.v3.oas.models.info.Contact;
import io.swagger.v3.oas.models.info.Info;

@EnableConfigurationProperties(ApplicationProperties.class)
@Configuration
public class ApplicationConfiguration {
  @Bean
  public OpenApiProperties openApiProperties(ApplicationProperties properties) {
    return properties.getApi();
  }

  @Bean
  public PipelineProperties pipelineProperties(ApplicationProperties properties) {
    return properties.getPipeline();
  }

  @Bean
  public MarketDataProperties marketDataProperties(ApplicationProperties properties) {
    return properties.getMarketData();
  }

  @Bean
  public StockPriceService stockPriceService(ApplicationProperties properties) {
    return new StockPriceService(properties);
  }

  @Bean
  public EventDecoder<MarketMessage> marketMesssageDecoder(WebSocketConfiguration configuration) {
    return new EventDecoder<>(MarketMessage.class, configuration.webSocketObjectMapper());
  }

  @Bean
  public EventEndpoint<MarketMessage> marketMesssageEndpoint(WebSocketConfiguration configuration,
    MarketDataProperties properties, Tracer tracer) {

    return new EventEndpoint<>(tracer, configuration.webSocketClient(), properties.getPricesApi(),
      properties.getErrorLimitToReconnect(), marketMesssageDecoder(null), stockPriceService(null));
  }

  @Configuration
  public static class OpenApiConfiguration {
    @Bean
    public OpenAPI openApi(OpenApiProperties properties) {
      final Contact contact = new Contact();
      properties.getContactName().ifPresent(contact::setName);
      properties.getContactUrl().ifPresent(contact::setUrl);
      properties.getContactEmail().ifPresent(contact::setEmail);

      final Info info = new Info()
        .title(properties.getTitle())
        .version(properties.getVersion())
        .contact(contact);
      properties.getDescription().ifPresent(info::setDescription);

      return new OpenAPI().info(info);
    }
  }

  @Configuration
  public static class WebSocketConfiguration {
    @Bean
    public WebSocketClient webSocketClient() {
      return new StandardWebSocketClient();
    }

    @Bean
    @Qualifier(Constants.QUALIFIER_WEBSOCKET)
    public ObjectMapper webSocketObjectMapper() {
      return ObjectMappers.webSocketObjectMapper();
    }
  }

  @Configuration
  public static class RestApiConfiguration {
    public static final String QUALIFIER = RestApiConfiguration.class.getSimpleName();

    @Bean
    public MappingJackson2HttpMessageConverter mappingJackson2HttpMessageConverter() {
      return new MappingJackson2HttpMessageConverter(restObjectMapper());
    }

    @Bean
    @Qualifier(Constants.QUALIFIER_REST)
    public ObjectMapper restObjectMapper() {
      return ObjectMappers.httpObjectMapper();
    }
  }
}
