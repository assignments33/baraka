package org.javamaker.assignment.baraka.util;

import java.time.Duration;
import java.time.temporal.Temporal;
import java.time.temporal.TemporalUnit;
import java.util.Objects;

public final class DurationUnit implements TemporalUnit {
  private static final int SECONDS_PER_DAY = 86400;
  private static final long NANOS_PER_SECOND = 1000_000_000L;
  private static final long NANOS_PER_DAY = NANOS_PER_SECOND * SECONDS_PER_DAY;

  private final Duration duration;

  private DurationUnit(Duration duration) {
    if (duration.isZero() || duration.isNegative()) {
      throw new IllegalArgumentException("Duration may not be zero or negative");
    }
    this.duration = duration;
  }

  public static DurationUnit of(Duration duration) {
    Objects.requireNonNull(duration, "duration must not be null");
    return new DurationUnit(duration);
  }

  @Override
  public Duration getDuration() {
    return duration;
  }

  public long toSeconds() {
    return duration.toSeconds();
  }

  @Override
  public boolean isDurationEstimated() {
    return (this.duration.getSeconds() >= SECONDS_PER_DAY);
  }

  @Override
  public boolean isDateBased() {
    return (duration.getNano() == 0 && duration.getSeconds() % SECONDS_PER_DAY == 0);
  }

  @Override
  public boolean isTimeBased() {
    return (duration.getSeconds() < SECONDS_PER_DAY && NANOS_PER_DAY % duration.toNanos() == 0);
  }

  @Override
  @SuppressWarnings("unchecked")
  public <R extends Temporal> R addTo(R temporal, long amount) {
    return (R) temporal.plus(amount, this);
  }

  @Override
  public long between(Temporal temporal1Inclusive, Temporal temporal2Exclusive) {
    return Duration.between(temporal1Inclusive, temporal2Exclusive).dividedBy(this.duration);
  }

  @Override
  public String toString() {
    return duration.toString();
  }
}
