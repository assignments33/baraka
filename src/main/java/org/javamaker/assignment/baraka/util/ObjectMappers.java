package org.javamaker.assignment.baraka.util;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateDeserializer;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateTimeDeserializer;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalTimeDeserializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateSerializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateTimeSerializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalTimeSerializer;

import lombok.experimental.UtilityClass;

@UtilityClass
public class ObjectMappers {

  public ObjectMapper httpObjectMapper() {
    return defaultMapper()
      .registerModule(isoTimeModule())
      .setSerializationInclusion(JsonInclude.Include.NON_EMPTY);
  }

  public ObjectMapper webSocketObjectMapper() {
    return defaultMapper()
      .registerModule(new JavaTimeModule())
      .disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES)
      .disable(DeserializationFeature.READ_DATE_TIMESTAMPS_AS_NANOSECONDS);
  }

  private ObjectMapper defaultMapper() {
    return new ObjectMapper()
      .enable(
        DeserializationFeature.ACCEPT_EMPTY_ARRAY_AS_NULL_OBJECT,
        DeserializationFeature.ACCEPT_EMPTY_STRING_AS_NULL_OBJECT,
        DeserializationFeature.READ_ENUMS_USING_TO_STRING)
      .disable(
        SerializationFeature.FAIL_ON_EMPTY_BEANS,
        SerializationFeature.INDENT_OUTPUT,
        SerializationFeature.WRITE_DATES_AS_TIMESTAMPS)
      .disable(JsonGenerator.Feature.WRITE_BIGDECIMAL_AS_PLAIN);
  }

  private com.fasterxml.jackson.databind.Module isoTimeModule() {
    return new JavaTimeModule()
      .addSerializer(LocalDate.class, new LocalDateSerializer(DateTimeFormatter.ISO_DATE))
      .addDeserializer(LocalDate.class, new LocalDateDeserializer(DateTimeFormatter.ISO_DATE))
      .addSerializer(LocalTime.class, new LocalTimeSerializer(DateTimeFormatter.ISO_TIME))
      .addDeserializer(LocalTime.class, new LocalTimeDeserializer(DateTimeFormatter.ISO_TIME))
      .addSerializer(LocalDateTime.class, new LocalDateTimeSerializer(DateTimeFormatter.ISO_DATE_TIME))
      .addDeserializer(LocalDateTime.class, new LocalDateTimeDeserializer(DateTimeFormatter.ISO_DATE_TIME));
  }

}
