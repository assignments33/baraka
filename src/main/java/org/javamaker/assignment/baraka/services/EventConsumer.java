package org.javamaker.assignment.baraka.services;

import java.util.function.Consumer;

/**
 * Consumer of incoming websocket data.
 *
 * @param <T> type of received messages
 */
@FunctionalInterface
public interface EventConsumer<T> extends Consumer<T> {}
