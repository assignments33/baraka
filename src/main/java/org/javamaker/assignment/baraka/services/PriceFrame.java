package org.javamaker.assignment.baraka.services;

import java.math.BigDecimal;
import java.time.Instant;
import java.util.Objects;
import java.util.function.BinaryOperator;

import lombok.Value;

/**
 * Single calculation frame related to a stock and time interval.
 */
@Value
class PriceFrame {
  Instant timestamp;
  BigDecimal minPrice;
  BigDecimal maxPrice;

  private PriceFrame(Instant timestamp, BigDecimal minPrice, BigDecimal maxPrice) {
    this.timestamp = timestamp;
    this.minPrice = minPrice;
    this.maxPrice = maxPrice;
  }

  static PriceFrame of(PipelineContext context, Instant epochTime, BigDecimal priceValue) {
    return new PriceFrame(truncateTime(context, epochTime), priceValue, priceValue);
  }

  boolean isAfter(PriceFrame otherEntry) {
    Objects.requireNonNull(otherEntry, () -> "otherEntry must not be null");
    return timestamp.isAfter(otherEntry.timestamp);
  }

  PriceFrame updateRange(PipelineContext context, Instant epochTime, BigDecimal priceValue) {
    final Instant timestamp = truncateTime(context, epochTime);

    if (Objects.equals(this.timestamp, timestamp)) {
      final BigDecimal minPrice = updatePrice(this.minPrice, priceValue, BigDecimal::min);
      final BigDecimal maxPrice = updatePrice(this.maxPrice, priceValue, BigDecimal::max);

      if (!Objects.equals(this.minPrice, minPrice) || !Objects.equals(this.maxPrice, maxPrice)) {
        return new PriceFrame(timestamp, minPrice, maxPrice);
      }

      return this;
    }

    return new PriceFrame(timestamp, priceValue, priceValue);
  }

  private static Instant truncateTime(PipelineContext context, Instant epochTime) {
    Objects.requireNonNull(context, () -> "context must not be null");
    return context.toFrameStart(epochTime);
  }

  private static BigDecimal updatePrice(BigDecimal initial, BigDecimal current, BinaryOperator<BigDecimal> func) {
    final BigDecimal result;
    if (current != null) {
      if (initial != null) {
        result = func.apply(initial, current);
      } else {
        result = current;
      }
    } else {
      result = initial;
    }
    return result;
  }
}
