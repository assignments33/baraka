package org.javamaker.assignment.baraka.services;

import java.math.BigDecimal;
import java.time.Instant;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Stream;

import org.javamaker.assignment.baraka.model.StockPrice;

import lombok.extern.slf4j.Slf4j;

/**
 * Min/max prices calculation pipeline.
 */
@Slf4j
public class PricePipeline {
  private final PipelineContext context;
  private final Map<String, PriceHistory> stockEntries;

  PricePipeline(PipelineContext context) {
    log.debug("<init>(): context = {}", context);
    this.context = Objects.requireNonNull(context, "context must noe bt null");
    this.stockEntries = new ConcurrentHashMap<>();
  }

  Stream<String> stockStream() {
    return stockEntries.keySet().stream();
  }

  Optional<PriceHistory> stockHistory(String stock) {
    return Optional.ofNullable(stockEntries.get(stock));
  }

  void pushPrice(StockPrice price) {
    if (log.isTraceEnabled()) {
      log.trace("pushPrice(): price = {}", price);
    }

    Optional.ofNullable(price)
      .map(StockPrice::getStock)
      .filter(context.stockFilter())
      .ifPresent(stock -> stockEntries.compute(stock, (k, v) -> updateHistory(v, price)));
  }

  private PriceHistory updateHistory(PriceHistory previous, StockPrice price) {
    if (log.isTraceEnabled()) {
      log.trace("updateHistory(): previous = {}, price = {}", previous, price);
    }

    final Instant epochTime = price.getTime();
    final BigDecimal priceValue = price.getPrice();

    if (previous != null) {
      if (log.isTraceEnabled()) {
        log.trace("UPDATE \"{}\": {} ==> \"{}\"",
          price.getStock(), epochTime, priceValue);
      }

      return previous.updateHistory(epochTime, priceValue);
    }

    if (log.isDebugEnabled()) {
      log.debug("CREATE \"{}\": {} ==> \"{}\"", price.getStock(), epochTime, priceValue);
    }

    return new PriceHistory(context, epochTime, priceValue);
  }
}
