package org.javamaker.assignment.baraka.services;

import java.time.temporal.ChronoUnit;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;

import org.apache.commons.collections4.CollectionUtils;
import org.javamaker.assignment.baraka.ApplicationProperties;
import org.javamaker.assignment.baraka.model.MarketMessage;
import org.javamaker.assignment.baraka.model.StockPriceRange;

import lombok.extern.slf4j.Slf4j;

/**
 * A service that handles events from the websocket and keeps calculation results.
 */
@Slf4j
public class StockPriceService implements EventConsumer<MarketMessage> {
  private final PricePipeline pipeline;

  public StockPriceService(ApplicationProperties properties) {
    Objects.requireNonNull(properties, "properties must not be null");

    final PipelineContext context = new PipelineContext(properties.getPipeline());
    this.pipeline = new PricePipeline(context);
  }

  public List<String> getStockNames() {
    return pipeline.stockStream()
      .sorted()
      .collect(Collectors.collectingAndThen(Collectors.toList(), List::copyOf));
  }

  public List<StockPriceRange> getStockRanges(String stock) {
    Objects.requireNonNull(stock, "stock must not be null");
    return pipeline.stockHistory(stock)
      .map(PriceHistory::getEntries)
      .stream()
      .flatMap(List::stream)
      .map(this::newRange)
      .collect(Collectors.toList());
  }

  private StockPriceRange newRange(PriceFrame input) {
    return StockPriceRange.builder()
      .startTime(input.getTimestamp())
      .endTime(input.getTimestamp().plus(59L, ChronoUnit.SECONDS))
      .minPrice(input.getMinPrice())
      .maxPrice(input.getMaxPrice())
      .build();
  }

  @Override
  public void accept(MarketMessage messsage) {
    if (log.isTraceEnabled()) {
      log.trace("accept(): messsage = {}", messsage);
    }
    Objects.requireNonNull(messsage, "message must not be null");

    Optional.of(messsage)
      .map(MarketMessage::getData)
      .filter(CollectionUtils::isNotEmpty)
      .stream()
      .flatMap(List::stream)
      .filter(Objects::nonNull)
      .forEach(pipeline::pushPrice);
  }
}
