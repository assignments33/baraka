package org.javamaker.assignment.baraka.services;

import java.net.URI;
import java.util.Objects;
import java.util.Optional;
import java.util.function.Consumer;
import java.util.stream.Stream;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.javamaker.assignment.baraka.Constants;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.cloud.sleuth.Span;
import org.springframework.cloud.sleuth.Tracer;
import org.springframework.context.Lifecycle;
import org.springframework.context.event.EventListener;
import org.springframework.web.socket.CloseStatus;
import org.springframework.web.socket.TextMessage;
import org.springframework.web.socket.WebSocketHandler;
import org.springframework.web.socket.WebSocketMessage;
import org.springframework.web.socket.WebSocketSession;
import org.springframework.web.socket.client.WebSocketClient;
import org.springframework.web.socket.client.WebSocketConnectionManager;

import lombok.extern.slf4j.Slf4j;

/**
 * Web-socket client endpoint.
 *
 * @param <T> type of received messages
 */
@Slf4j
public class EventEndpoint<T> implements Lifecycle {
  private final WebSocketConnectionManager connectionManager;
  private final EventDecoder<T> decoder;
  private final EventConsumer<T> consumer;
  private final int errorLimit;
  private int errorCount = 0;

  public EventEndpoint(Tracer tracer, WebSocketClient client, URI websocketUri, int errorLimit,
    EventDecoder<T> decoder, EventConsumer<T> consumer) {
    log.debug("<init>(): client = {}, websocketUri = {}, decoder = {}, consumer = {}",
      client, websocketUri, decoder, consumer);
    Objects.requireNonNull(websocketUri, "websocketUri must not be null");

    this.errorLimit = errorLimit;
    this.decoder = Objects.requireNonNull(decoder, "decoder must not be null");
    this.consumer = Objects.requireNonNull(consumer, "consumer must not be null");

    this.connectionManager = new WebSocketConnectionManager(client,
      new EndpointHandler(websocketUri, tracer), websocketUri.toString());
  }

  @EventListener(ApplicationReadyEvent.class)
  @Override
  public void start() {
    log.info("Starting endpoint...");
    doOpen(false);
  }

  @Override
  public void stop() {
    log.info("Stopping endpoint...");
    doClose();
  }

  @Override
  public boolean isRunning() {
    return connectionManager.isRunning();
  }

  private void doOpen(boolean close) {
    if (close) {
      doClose();
    }

    connectionManager.start();
  }

  private void doClose() {
    connectionManager.stop();
  }

  private void onMessage(TextMessage message) {
    Objects.requireNonNull(message, "message must not be null");
    Optional.of(message)
      .map(TextMessage::getPayload)
      .filter(StringUtils::isNotBlank)
      .map(decoder)
      .ifPresent(consumer);
  }

  private void onClosed(CloseStatus status) {
    final boolean shouldReconnect = Stream.of(
      CloseStatus.NO_CLOSE_FRAME,
      CloseStatus.SERVER_ERROR,
      CloseStatus.SERVICE_RESTARTED)
      .anyMatch(element -> element.equals(status));

    if (shouldReconnect) {
      log.info("Restarting endpoint...");
      doOpen(true);
    } else {
      stop();
    }
  }

  private void onError(Throwable exception) {
    if (errorLimit > 0) {
      ++errorCount;
      if (errorCount >= errorLimit) {
        log.info("Error limit of {} exceeded, endpoint will be restarted", errorLimit);
        doOpen(true);
      }
    } else {
      log.warn("Error limit isn't set, skiping exception: {}", ExceptionUtils.getMessage(exception));
    }
  }

  private final class EndpointHandler implements WebSocketHandler {
    private final URI uri;
    private final Tracer tracer;

    public EndpointHandler(URI uri, Tracer tracer) {
      log.debug("<init>(): uri = {}, tracer = {}", uri, tracer);

      this.uri = uri;
      this.tracer = tracer;
    }

    @Override
    public void afterConnectionEstablished(WebSocketSession session) throws Exception {
      tracedOperation("afterConnectionEstablished", span -> {
        span.event(Constants.TRACE_EVENT_ENDPOINT_CONNECTED);
        log.info("[{}] Connected to \"{}\"", session.getId(), uri);
      });
    }

    @Override
    public void afterConnectionClosed(WebSocketSession session, CloseStatus status) {
      tracedOperation("afterConnectionClosed", span -> {
        span.event(Constants.TRACE_EVENT_ENDPOINT_DISCONNECTED);
        span.tag(Constants.TRACE_TAG_STATUS_CODE, "" + status.getCode());
        span.tag(Constants.TRACE_TAG_STATUS_REASON, status.getReason());
        log.info("[{}] Disconnected from \"{}\"", session.getId(), uri);
        onClosed(status);
      });
    }

    @Override
    public void handleMessage(WebSocketSession session, WebSocketMessage<?> message) {
      tracedOperation("handleMessage", span -> {
        span.event(Constants.TRACE_EVENT_MESSAGE_RECEIVED);

        if (log.isTraceEnabled()) {
          log.trace("[{}] handleMessage(): message = {}", session.getId(), message);
        }

        if (message instanceof final TextMessage textMessage) {
          try {
            onMessage(textMessage);
            span.event(Constants.TRACE_EVENT_MESSAGE_PROCESSED);

          } catch (IllegalArgumentException | NullPointerException e) {
            log.error("[{}] ERROR processing message \"{}\"", session.getId(), message.getPayload(), e);
            span.error(e)
              .event(Constants.TRACE_EVENT_MESSAGE_FAILURE)
              .tag(Constants.TRACE_TAG_EXCEPTION, ExceptionUtils.getMessage(e));
          }

        } else {
          final String errorInfo = Optional.ofNullable(message)
            .map(Object::getClass)
            .map(Class::getName)
            .map(name -> "Unexpected message type: " + name)
            .orElseGet(() -> "Empty message payload");

          log.warn(errorInfo);
          span.event(Constants.TRACE_EVENT_MESSAGE_FAILURE)
            .tag(Constants.TRACE_TAG_EXCEPTION, errorInfo);
        }
      });
    }

    @Override
    public void handleTransportError(WebSocketSession session, Throwable exception) {
      tracedOperation("afterConnectionClosed", span -> {
        span.error(exception)
          .event(Constants.TRACE_EVENT_ENDPOINT_EXCEPTION)
          .tag(Constants.TRACE_TAG_EXCEPTION, ExceptionUtils.getMessage(exception));
        log.error("[{}] Transport ERROR occured for \"{}\"", session.getId(), uri, exception);
        onError(exception);
      });

      onError(exception);
    }

    @Override
    public boolean supportsPartialMessages() {
      return false;
    }

    private void tracedOperation(String operationName, Consumer<Span> operation) {
      Objects.requireNonNull(operation, () -> "operation must not be null");

      final Span span = tracer.nextSpan().name(operationName);
      try (Tracer.SpanInScope spanScope = tracer.withSpan(span)) {
        operation.accept(span);
      } finally {
        span.end();
      }
    }
  }
}
