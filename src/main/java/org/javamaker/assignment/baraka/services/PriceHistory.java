package org.javamaker.assignment.baraka.services;

import java.math.BigDecimal;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import lombok.extern.slf4j.Slf4j;

/**
 * Collection of frames related to a stock on a time-scale.
 */
@Slf4j
class PriceHistory {
  private final PipelineContext context;
  private final PriceFrame current;
  private final List<PriceFrame> history;

  PriceHistory(PipelineContext context, Instant epochTime, BigDecimal priceValue) {
    this(context, PriceFrame.of(context, epochTime, priceValue), List.of());
  }

  private PriceHistory(PipelineContext context, PriceFrame current, List<PriceFrame> history) {
    if (log.isTraceEnabled()) {
      log.trace("<init>(): context = {}, current = {}, history = {}", context, current, history);
    }

    this.context = Objects.requireNonNull(context, () -> "context must not be null");
    this.current = Objects.requireNonNull(current, () -> "current must not be null");
    this.history = Objects.requireNonNull(history, () -> "history must not be null");
  }

  List<PriceFrame> getEntries() {
    final int historyLimit = context.historySize();
    final List<PriceFrame> output = new ArrayList<>(historyLimit);

    final int historySize = history.size();
    if (historySize < historyLimit) {
      output.addAll(history);
    } else {
      final int depthStart = historySize - historyLimit;
      output.addAll(history.subList(depthStart + 1, historySize));
    }

    output.add(current);
    return List.copyOf(output);
  }

  PriceHistory updateHistory(Instant epochTime, BigDecimal priceValue) {
    final PriceFrame newEntry = current.updateRange(context, epochTime, priceValue);

    if (newEntry.isAfter(current)) {
      if (log.isTraceEnabled()) {
        log.trace("... moving frame: {}", newEntry);
      }
      return new PriceHistory(context, newEntry, getEntries());
    }

    if (!newEntry.equals(current)) {
      if (log.isTraceEnabled()) {
        log.trace("... updating frame: {}", newEntry);
      }
      return new PriceHistory(context, newEntry, history);
    }

    return this;
  }
}
