package org.javamaker.assignment.baraka.services;

import java.time.Duration;
import java.time.Instant;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import org.apache.commons.lang3.StringUtils;
import org.javamaker.assignment.baraka.ApplicationProperties.PipelineProperties;
import org.javamaker.assignment.baraka.util.DurationUnit;

/**
 * A set of configurable traits and policies used in min/max calculation pipeline.
 */
class PipelineContext {
  private final DurationUnit frameDuration;
  private final long frameShiftSeconds;
  private final int frameHistorySize;
  private final Predicate<String> stockFilter;

  public PipelineContext(PipelineProperties properties) {
    Objects.requireNonNull(properties, "properties must not be null");

    final int frameHistory = properties.getFrameHistory();
    if (frameHistory <= 0) {
      throw new IllegalArgumentException("properties.frameHistory must be > 0");
    }

    frameDuration = DurationUnit.of(properties.getFrameDuration());
    frameShiftSeconds = sanitizeShiftSeconds(properties.getFrameShift(), frameDuration);
    frameHistorySize = frameHistory;
    stockFilter = configureStockFilter(properties.getStockFilter());
  }

  private Predicate<String> configureStockFilter(List<String> stockList) {
    final Set<String> allowedStocks = Optional.ofNullable(stockList)
      .stream()
      .flatMap(List::stream)
      .filter(StringUtils::isNotBlank)
      .collect(Collectors.collectingAndThen(Collectors.toSet(), Set::copyOf));

    if (allowedStocks.isEmpty()) {
      return name -> true;
    }

    return name -> name != null && allowedStocks.contains(name);
  }

  int historySize() {
    return frameHistorySize;
  }

  Predicate<String> stockFilter() {
    return stockFilter;
  }

  Instant toFrameStart(Instant epochTime) {
    Objects.requireNonNull(epochTime, "epochTime must not be null");
    return epochTime.minusSeconds(frameShiftSeconds)
      .truncatedTo(frameDuration)
      .plusSeconds(frameShiftSeconds);
  }

  Instant toFrameEnd(Instant epochTime) {
    return toFrameStart(epochTime).plusSeconds(frameDuration.toSeconds() - 1);
  }

  private static long sanitizeShiftSeconds(Optional<Duration> shiftDuration, DurationUnit frameDuration) {
    final Duration windowShift = shiftDuration.orElseGet(() -> Duration.ZERO);
    return windowShift.toSeconds() % frameDuration.toSeconds();
  }
}
