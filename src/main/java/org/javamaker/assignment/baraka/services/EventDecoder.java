package org.javamaker.assignment.baraka.services;

import java.io.IOException;
import java.util.Objects;
import java.util.function.Function;

import org.springframework.core.convert.ConversionFailedException;
import org.springframework.core.convert.TypeDescriptor;

import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * JSON decoder for websocket data.
 *
 * @param <T> type of decoded messages
 */
public class EventDecoder<T> implements Function<String, T> {
  private final Class<T> eventType;
  private final ObjectMapper objectMapper;

  public EventDecoder(Class<T> eventType, ObjectMapper objectMapper) {
    this.eventType = Objects.requireNonNull(eventType, () -> "eventType must not be null");
    this.objectMapper = Objects.requireNonNull(objectMapper, () -> "objectMapper must not be null");
  }

  @Override
  public T apply(String value) {
    try {
      return objectMapper.readValue(value, eventType);
    } catch (final IOException e) {
      throw new ConversionFailedException(TypeDescriptor.valueOf(String.class),
        TypeDescriptor.valueOf(eventType), value, e);
    }
  }
}
