package org.javamaker.assignment.baraka;

import java.net.URI;
import java.time.Duration;
import java.util.List;
import java.util.Optional;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.validation.annotation.Validated;

import lombok.AccessLevel;
import lombok.Data;
import lombok.experimental.FieldDefaults;

/**
 * Configuration properties for the service.
 */
@Data
@FieldDefaults(level = AccessLevel.PRIVATE)
@Validated
@ConfigurationProperties(ApplicationProperties.PREFIX)
public final class ApplicationProperties {
  public static final String PREFIX = "application";

  @Valid
  @NotNull
  OpenApiProperties api;
  @Valid
  @NotNull
  MarketDataProperties marketData;
  @Valid
  @NotNull
  PipelineProperties pipeline;

  @Data
  @FieldDefaults(level = AccessLevel.PRIVATE)
  public static final class OpenApiProperties {
    /** Service name */
    @NotBlank
    String title;
    /** Service summary */
    Optional<String> description = Optional.empty();
    /** Service version */
    @NotBlank
    String version;
    /** Service creator name */
    Optional<String> contactName = Optional.empty();
    /** Creator home page URL */
    Optional<String> contactUrl = Optional.empty();
    /** Creator mail address */
    Optional<String> contactEmail = Optional.empty();
  }

  @Data
  @FieldDefaults(level = AccessLevel.PRIVATE)
  public static final class MarketDataProperties {
    /** Default value for the {@link MarketDataProperties#getErrorLimitToReconnect()}. */
    public static final int DEFAULT_ERROR_LIMIT_TO_RECONNECT = 0;

    /** URI of the stream of prices. */
    @NotNull
    URI pricesApi;
    /** The number of errors to wait before reconnecting to the feed, default value is 0 which means no limit. */
    int errorLimitToReconnect = DEFAULT_ERROR_LIMIT_TO_RECONNECT;
  }

  @Data
  @FieldDefaults(level = AccessLevel.PRIVATE)
  public static final class PipelineProperties {
    /** Default value for the {@link PipelineProperties#getHistoryDepth()} property. */
    public static final int DEFAULT_FRAME_HISTORY = 10;
    /** Default value for the {@link PipelineProperties#getWindowInterval()}. */
    public static final Duration DEFAULT_FRAME_DURATION = Duration.ofMinutes(1);

    /**
     * Duration of time-scale interval, used for data aggregation.
     * <p>
     * A second is the minimum unit of the scale. Default value is 60 seconds.
     */
    @NotNull
    Duration frameDuration = DEFAULT_FRAME_DURATION;
    /**
     * Shift of the time-scale, second precision is supported.
     * <p>
     * Default value is empty which means no shift.
     */
    @NotNull
    Optional<Duration> frameShift = Optional.empty();

    /** Number of time-scale intervals to keep in memory, default value is 10 frames */
    @Positive
    int frameHistory = DEFAULT_FRAME_HISTORY;

    /**
     * List of allowed stock names, if this list is not empty pipeline filtering is enabled.
     * <p>
     * By default this value is empty which means filtering is disabled.
     */
    List<String> stockFilter;
  }
}
