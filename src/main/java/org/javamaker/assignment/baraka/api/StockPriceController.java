package org.javamaker.assignment.baraka.api;

import java.util.List;

import javax.validation.constraints.NotBlank;

import org.javamaker.assignment.baraka.model.StockPriceRange;
import org.javamaker.assignment.baraka.services.StockPriceService;
import org.springframework.http.MediaType;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
@Validated
@RestController
@RequestMapping(path = "/api/", produces = MediaType.APPLICATION_JSON_VALUE)
public class StockPriceController {
  private final StockPriceService service;

  @Operation(summary = "Show stocks", description = "Get a list of available stock symbols")
  @GetMapping("/stock")
  public List<String> getStockList() { return service.getStockNames(); }

  @Operation(summary = "Show stock prices", description = "Get a list of Min/Max prices of an asked stock")
  @GetMapping("/stock/{name}/prices")
  public List<StockPriceRange> getStockRanges(
    @Parameter(description = "A stock (symbol) name") //
    @PathVariable("name") @NotBlank String stockName) {
    return service.getStockRanges(stockName);
  }
}
