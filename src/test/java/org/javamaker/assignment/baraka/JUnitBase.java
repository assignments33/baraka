package org.javamaker.assignment.baraka;

import java.lang.reflect.Method;
import java.math.BigDecimal;
import java.time.Duration;
import java.time.Instant;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import org.apache.commons.lang3.ArrayUtils;
import org.javamaker.assignment.baraka.ApplicationProperties.PipelineProperties;
import org.javamaker.assignment.baraka.model.MarketMessage;
import org.javamaker.assignment.baraka.model.StockPrice;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.TestInfo;
import org.junit.jupiter.api.extension.ExtendWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.test.context.ActiveProfiles;

import lombok.Builder;

@ActiveProfiles("test")
@ExtendWith(ExceptionLogger.class)
public class JUnitBase {

  @BeforeEach
  void logStart(TestInfo info) {
    testLog().info("BEGIN \"{}\"", testName(info));
  }

  @AfterEach
  void logFinish(TestInfo info) {
    testLog().info("END \"{}\"", testName(info));
  }

  private Logger testLog() {
    return LoggerFactory.getLogger(getClass());
  }

  private String testName(TestInfo info) {
    return Optional.ofNullable(info)
      .flatMap(TestInfo::getTestMethod)
      .map(Method::getName)
      .orElse("");
  }

  @Builder(builderMethodName = "pipelineProperties")
  protected static PipelineProperties newProperties(Duration frameDuration,
    Duration frameShift, int frameHistory, List<String> stockFilter) {

    final PipelineProperties properties = new PipelineProperties();
    if (frameDuration != null) {
      properties.setFrameDuration(frameDuration);
    }
    properties.setFrameShift(Optional.ofNullable(frameShift));
    if (frameHistory > 0) {
      properties.setFrameHistory(frameHistory);
    }
    properties.setStockFilter(stockFilter);
    return properties;
  }

  protected static PipelineProperties defaultPipelineProperties() {
    return pipelineProperties()
      .frameDuration(Duration.ofMinutes(1))
      .frameShift(Duration.ZERO)
      .build();
  }

  protected static MarketMessage marketMesssage(StockPrice... priceList) {
    final MarketMessage message = new MarketMessage();
    message.setType("trade");
    if (ArrayUtils.isNotEmpty(priceList)) {
      message.setData(Arrays.asList(priceList));
    }
    return message;
  }

  protected static StockPrice stockPrice(String stock, String time, String value) {
    final StockPrice price = new StockPrice();
    price.setStock(stock);
    price.setTime(Instant.parse(time));
    price.setPrice(new BigDecimal(value));
    return price;
  }
}
