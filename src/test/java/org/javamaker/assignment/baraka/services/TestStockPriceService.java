package org.javamaker.assignment.baraka.services;

import static org.assertj.core.api.Assertions.*;

import java.math.BigDecimal;
import java.time.Instant;
import java.util.List;

import org.javamaker.assignment.baraka.ApplicationProperties;
import org.javamaker.assignment.baraka.JUnitBase;
import org.javamaker.assignment.baraka.model.StockPriceRange;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

@DisplayName("StockPriceService")
class TestStockPriceService extends JUnitBase {
  private ApplicationProperties properties;
  private StockPriceService service;

  @BeforeEach
  void setUp() {
    properties = new ApplicationProperties();
    properties.setPipeline(defaultPipelineProperties());

    service = new StockPriceService(properties);
  }

  @DisplayName("<init>(ApplicationProperties)")
  @Nested
  class TestCreate {
    @DisplayName("null properties")
    @Test
    void testNullProperties() {
      assertThatNullPointerException()
        .isThrownBy(() -> new StockPriceService(null))
        .withMessageContainingAll("properties", "must not be null");
    }

    @DisplayName("success path")
    @Test
    void testAllArguments() {
      assertThatNoException().isThrownBy(() -> new StockPriceService(properties));
    }
  }

  @DisplayName("getStockNames()")
  @Nested
  class TestGetStockNames {
    @DisplayName("empty")
    @Test
    void testEmpty() {
      assertThat(service.getStockNames()).isEmpty();
    }

    @DisplayName("not empty")
    @Test
    void testNotEmpty() {
      service.accept(marketMesssage());
      assertThat(service.getStockNames()).isEmpty();

      service.accept(marketMesssage(stockPrice("IBM", "2022-05-26T11:42:56.424Z", "58.15716")));
      service.accept(marketMesssage(stockPrice("AMD", "2022-05-26T11:45:12.531Z", "61.84613")));
      assertThat(service.getStockNames()).hasSize(2).containsAll(List.of("IBM", "AMD"));

      service.accept(marketMesssage(
        stockPrice("IBM", "2022-05-26T11:43:06.054Z", "58.71602"),
        stockPrice("AMD", "2022-05-26T11:45:57.103Z", "60.00739")));
      assertThat(service.getStockNames()).hasSize(2).containsAll(List.of("IBM", "AMD"));
    }
  }

  @DisplayName("getStockRanges(String)")
  @Nested
  class TestGetStockRanges {
    @DisplayName("null stock")
    @Test
    void testNullProperties() {
      assertThatNullPointerException()
        .isThrownBy(() -> service.getStockRanges(null))
        .withMessageContainingAll("stock", "must not be null");
    }

    @DisplayName("empty")
    @Test
    void testEmpty() {
      assertThat(service.getStockRanges("IBM")).isEmpty();
    }

    @DisplayName("success path")
    @Test
    void testSuccessPath() {
      service.accept(marketMesssage());
      assertThat(service.getStockNames()).isEmpty();

      service.accept(marketMesssage(
        stockPrice("IBM", "2022-05-26T11:42:26.424Z", "58.15716"),
        stockPrice("IBM", "2022-05-26T11:42:57.103Z", "60.00739")));

      assertThat(service.getStockRanges("AMD")).isEmpty();
      assertThat(service.getStockRanges("IBM"))
        .hasSize(1)
        .containsExactly(
          StockPriceRange.builder()
            .startTime(Instant.parse("2022-05-26T11:42:00Z"))
            .endTime(Instant.parse("2022-05-26T11:42:59Z"))
            .minPrice(new BigDecimal("58.15716"))
            .maxPrice(new BigDecimal("60.00739"))
            .build());

      service.accept(marketMesssage(
        stockPrice("IBM", "2022-05-26T11:43:10.002Z", "57.32671"),
        stockPrice("IBM", "2022-05-26T11:44:57.103Z", "55.49173")));
      service.accept(marketMesssage(
        stockPrice("IBM", "2022-05-26T11:44:59.447Z", "52.85193")));
      assertThat(service.getStockRanges("IBM"))
        .hasSize(3)
        .satisfies(rangeList -> {
          assertThat(rangeList.get(1)).isEqualTo(
            StockPriceRange.builder()
              .startTime(Instant.parse("2022-05-26T11:43:00Z"))
              .endTime(Instant.parse("2022-05-26T11:43:59Z"))
              .minPrice(new BigDecimal("57.32671"))
              .maxPrice(new BigDecimal("57.32671"))
              .build());
          assertThat(rangeList.get(2)).isEqualTo(
            StockPriceRange.builder()
              .startTime(Instant.parse("2022-05-26T11:44:00Z"))
              .endTime(Instant.parse("2022-05-26T11:44:59Z"))
              .minPrice(new BigDecimal("52.85193"))
              .maxPrice(new BigDecimal("55.49173"))
              .build());
        });
    }
  }

  @DisplayName("accept(MarketMessage)")
  @Nested
  class TestAccept {
    @DisplayName("null message")
    @Test
    void testNullMessage() {
      assertThatNullPointerException()
        .isThrownBy(() -> service.accept(null))
        .withMessageContainingAll("message", "must not be null");
    }

    @DisplayName("stock filtering")
    @Test
    void testStockFiltering() {
      properties.getPipeline().setStockFilter(List.of("INTL"));
      service = new StockPriceService(properties);

      service.accept(marketMesssage(
        stockPrice("IBM", "2022-05-26T11:42:26.424Z", "58.15716"),
        stockPrice("AMD", "2022-05-26T11:42:57.103Z", "60.00739")));
      assertThat(service.getStockNames()).isEmpty();

      service.accept(marketMesssage(
        stockPrice("INTL", "2022-05-26T11:44:59.447Z", "52.85193")));
      assertThat(service.getStockNames()).containsExactly("INTL");
    }
  }
}
