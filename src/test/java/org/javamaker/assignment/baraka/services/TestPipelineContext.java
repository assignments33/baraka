package org.javamaker.assignment.baraka.services;

import static org.assertj.core.api.Assertions.*;
import static org.junit.jupiter.api.Assertions.*;

import java.time.Duration;
import java.time.Instant;
import java.util.List;

import org.javamaker.assignment.baraka.ApplicationProperties.PipelineProperties;
import org.javamaker.assignment.baraka.JUnitBase;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

@DisplayName("PipelineContext")
class TestPipelineContext extends JUnitBase {

  @DisplayName("<init>(PipelineProperties)")
  @Nested
  class TestCreate {
    @DisplayName("null properties")
    @Test
    void testNullProperties() {
      assertThatNullPointerException()
        .isThrownBy(() -> new PipelineContext(null))
        .withMessageContainingAll("properties", "must not be null");
    }

    @DisplayName("null properties.frameDuration")
    @Test
    void testNullPropertiesFrameDuration() {
      final PipelineProperties invalidProperties = new PipelineProperties();
      invalidProperties.setFrameDuration(null);

      assertThatNullPointerException()
        .isThrownBy(() -> new PipelineContext(invalidProperties))
        .withMessageContainingAll("duration", "must not be null");
    }

    @DisplayName("success path")
    @Test
    void testSuccess() {
      assertThatNoException().isThrownBy(() -> new PipelineContext(new PipelineProperties()));
    }
  }

  @DisplayName("historySize()")
  @Nested
  class TestHistorySize {

    @DisplayName("default")
    @Test
    void testDefault() {
      final PipelineContext context = new PipelineContext(new PipelineProperties());
      assertEquals(PipelineProperties.DEFAULT_FRAME_HISTORY, context.historySize());
    }

    @DisplayName("configured")
    @Test
    void testConfigured() {
      final PipelineContext context = new PipelineContext(
        pipelineProperties()
          .frameHistory(555)
          .build());
      assertEquals(555, context.historySize());
    }
  }

  @DisplayName("stockFilter()")
  @Nested
  class TestStockFilter {
    @DisplayName("default")
    @Test
    void testDefault() {
      final PipelineContext context = new PipelineContext(new PipelineProperties());
      assertThat(context.stockFilter()).accepts("any", "value", " ", null);
    }

    @DisplayName("configured")
    @Test
    void testConfigured() {
      final PipelineContext context = new PipelineContext(
        pipelineProperties()
          .stockFilter(List.of("AMD", "IBM"))
          .build());
      assertThat(context.stockFilter()).accepts("AMD", "IBM");
      assertThat(context.stockFilter()).rejects("ABT", null);
    }
  }

  @DisplayName("toFrameStart(Instant)")
  @Nested
  class TestToFrameStart {
    @DisplayName("null epochTime")
    @Test
    void testNullEpochTime() {
      final PipelineContext context = new PipelineContext(new PipelineProperties());
      assertThatNullPointerException()
        .isThrownBy(() -> context.toFrameStart(null))
        .withMessageContainingAll("epochTime", "must not be null");
    }

    @DisplayName("duration - minutes")
    @Test
    void testDurationMunites() {
      final Duration frameDuration = Duration.ofMinutes(5);
      final PipelineContext context = new PipelineContext(
        pipelineProperties()
          .frameDuration(frameDuration)
          .build());

      assertEquals(Instant.parse("2022-05-26T11:40:00Z"),
        context.toFrameStart(Instant.parse("2022-05-26T11:42:56.424Z")));

      assertEquals(Instant.parse("2022-05-26T11:45:00Z"),
        context.toFrameStart(Instant.parse("2022-05-26T11:47:53Z")));

      assertEquals(Instant.parse("2022-05-26T11:40:00Z"),
        context.toFrameStart(Instant.parse("2022-05-26T11:40:00Z")));
    }

    @DisplayName("duration - seconds")
    @Test
    void testDurationSeconds() {
      final Duration frameDuration = Duration.ofSeconds(12);
      final PipelineContext context = new PipelineContext(
        pipelineProperties()
          .frameDuration(frameDuration)
          .build());

      assertEquals(Instant.parse("2022-05-26T11:56:36Z"),
        context.toFrameStart(Instant.parse("2022-05-26T11:56:41.424Z")));

      assertEquals(Instant.parse("2022-05-26T11:45:48Z"),
        context.toFrameStart(Instant.parse("2022-05-26T11:45:53Z")));

      assertEquals(Instant.parse("2022-05-26T11:40:00Z"),
        context.toFrameStart(Instant.parse("2022-05-26T11:40:00Z")));

      assertEquals(Instant.parse("2022-05-26T11:06:12Z"),
        context.toFrameStart(Instant.parse("2022-05-26T11:06:12Z")));
    }

    @DisplayName("duration - minutes with shift")
    @Test
    void testDurationMunitesWithShift() {
      final Duration frameDuration = Duration.ofMinutes(1);
      final PipelineContext context = new PipelineContext(
        pipelineProperties()
          .frameDuration(frameDuration)
          .frameShift(Duration.ofSeconds(30))
          .build());

      assertEquals(Instant.parse("2022-05-26T11:42:30Z"),
        context.toFrameStart(Instant.parse("2022-05-26T11:42:56.424Z")));

      assertEquals(Instant.parse("2022-05-26T11:42:30Z"),
        context.toFrameStart(Instant.parse("2022-05-26T11:43:00.562Z")));

      assertEquals(Instant.parse("2022-05-26T11:42:30Z"),
        context.toFrameStart(Instant.parse("2022-05-26T11:43:05.562Z")));

      assertEquals(Instant.parse("2022-05-26T11:43:30Z"),
        context.toFrameStart(Instant.parse("2022-05-26T11:43:31.562Z")));
    }
  }

  @DisplayName("toFrameEnd(Instant)")
  @Nested
  class TestToFrameEnd {
    @DisplayName("null epochTime")
    @Test
    void testNullEpochTime() {
      final PipelineContext context = new PipelineContext(new PipelineProperties());
      assertThatNullPointerException()
        .isThrownBy(() -> context.toFrameEnd(null))
        .withMessageContainingAll("epochTime", "must not be null");
    }

    @DisplayName("duration - minutes")
    @Test
    void testDurationMunites() {
      final Duration frameDuration = Duration.ofMinutes(5);
      final PipelineContext context = new PipelineContext(
        pipelineProperties()
          .frameDuration(frameDuration)
          .build());

      assertEquals(Instant.parse("2022-05-26T11:44:59Z"),
        context.toFrameEnd(Instant.parse("2022-05-26T11:42:56.424Z")));

      assertEquals(Instant.parse("2022-05-26T11:49:59Z"),
        context.toFrameEnd(Instant.parse("2022-05-26T11:47:53Z")));

      assertEquals(Instant.parse("2022-05-26T11:44:59Z"),
        context.toFrameEnd(Instant.parse("2022-05-26T11:40:00Z")));
    }

    @DisplayName("duration - seconds")
    @Test
    void testDurationSeconds() {
      final Duration frameDuration = Duration.ofSeconds(12);
      final PipelineContext context = new PipelineContext(
        pipelineProperties()
          .frameDuration(frameDuration)
          .build());

      assertEquals(Instant.parse("2022-05-26T11:56:47Z"),
        context.toFrameEnd(Instant.parse("2022-05-26T11:56:41.424Z")));

      assertEquals(Instant.parse("2022-05-26T11:45:59Z"),
        context.toFrameEnd(Instant.parse("2022-05-26T11:45:53Z")));

      assertEquals(Instant.parse("2022-05-26T11:40:11Z"),
        context.toFrameEnd(Instant.parse("2022-05-26T11:40:00Z")));

      assertEquals(Instant.parse("2022-05-26T11:06:23Z"),
        context.toFrameEnd(Instant.parse("2022-05-26T11:06:12Z")));
    }

    @DisplayName("duration - minutes with shift")
    @Test
    void testDurationMunitesWithShift() {
      final Duration frameDuration = Duration.ofMinutes(1);
      final PipelineContext context = new PipelineContext(
        pipelineProperties()
          .frameDuration(frameDuration)
          .frameShift(Duration.ofSeconds(30))
          .build());

      assertEquals(Instant.parse("2022-05-26T11:43:29Z"),
        context.toFrameEnd(Instant.parse("2022-05-26T11:42:56.424Z")));

      assertEquals(Instant.parse("2022-05-26T11:43:29Z"),
        context.toFrameEnd(Instant.parse("2022-05-26T11:43:00.562Z")));

      assertEquals(Instant.parse("2022-05-26T11:43:29Z"),
        context.toFrameEnd(Instant.parse("2022-05-26T11:43:05.562Z")));

      assertEquals(Instant.parse("2022-05-26T11:44:29Z"),
        context.toFrameEnd(Instant.parse("2022-05-26T11:43:31.562Z")));
    }
  }
}
