package org.javamaker.assignment.baraka.services;

import static org.assertj.core.api.Assertions.*;

import java.math.BigDecimal;
import java.time.Instant;

import org.javamaker.assignment.baraka.ApplicationProperties.PipelineProperties;
import org.javamaker.assignment.baraka.JUnitBase;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

@DisplayName("PriceHistory")
class TestPriceHistory extends JUnitBase {
  private PipelineContext context;

  @BeforeEach
  void setUp() {
    context = new PipelineContext(defaultPipelineProperties());
  }

  @DisplayName("<init>(PipelineContext, Instant, BigDecimal)")
  @Nested
  class TestCreate {
    @DisplayName("null context")
    @Test
    void testNullContext() {
      assertThatNullPointerException()
        .isThrownBy(() -> new PriceHistory(null, null, null))
        .withMessageContainingAll("context", "must not be null");
    }

    @DisplayName("null epochTime")
    @Test
    void testEpochTime() {
      assertThatNullPointerException()
        .isThrownBy(() -> new PriceHistory(context, null, BigDecimal.ZERO))
        .withMessageContainingAll("epochTime", "must not be null");
    }

    @DisplayName("null priceValue")
    @Test
    void testPriceValue() {
      assertThatNoException().isThrownBy(() -> new PriceHistory(context, Instant.now(), null));
    }

    @DisplayName("success path")
    @Test
    void testAllArguments() {
      assertThatNoException().isThrownBy(() -> new PriceHistory(context, Instant.now(), BigDecimal.TEN));
    }
  }

  @DisplayName("getEntries()")
  @Test
  void testGetEntries() {
    final PriceHistory history1 = new PriceHistory(context,
      Instant.parse("2022-05-26T11:42:56.424Z"), new BigDecimal("58.15716"));

    final Instant expectedFrameTime = Instant.parse("2022-05-26T11:42:00Z");
    assertThat(history1.getEntries())
      .hasSize(1)
      .first()
      .returns(expectedFrameTime, PriceFrame::getTimestamp)
      .returns(new BigDecimal("58.15716"), PriceFrame::getMinPrice)
      .returns(new BigDecimal("58.15716"), PriceFrame::getMaxPrice);

    final PriceHistory history2 = history1.updateHistory(
      Instant.parse("2022-05-26T11:45:12.531Z"), new BigDecimal("61.84613"));

    assertThat(history2.getEntries())
      .hasSize(2)
      .containsExactly(
        priceFrame("2022-05-26T11:42:00Z", "58.15716"),
        priceFrame("2022-05-26T11:45:00Z", "61.84613"));
  }

  @DisplayName("updateHistory(Instant, BigDecimal)")
  @Nested
  class TestUpdateHistory {
    @DisplayName("null epochTime")
    @Test
    void testEpochTime() {
      final PriceHistory history = new PriceHistory(context, Instant.now(), null);
      assertThatNullPointerException()
        .isThrownBy(() -> history.updateHistory(null, BigDecimal.TEN))
        .withMessageContainingAll("epochTime", "must not be null");
    }

    @DisplayName("null priceValue")
    @Test
    void testPriceValue() {
      final PriceHistory history = new PriceHistory(context, Instant.now(), BigDecimal.TEN);
      assertThatNoException().isThrownBy(() -> history.updateHistory(Instant.now().plusSeconds(5), null));
    }

    @DisplayName("history limit")
    @Test
    void testHistoryLimit() {
      final PipelineProperties properties = defaultPipelineProperties();
      properties.setFrameHistory(2);
      context = new PipelineContext(properties);

      final PriceHistory history1 = new PriceHistory(context,
        Instant.parse("2022-05-26T11:42:56.424Z"), new BigDecimal("58.15716"));

      assertThat(history1.getEntries())
        .hasSize(1)
        .first()
        .isEqualTo(priceFrame("2022-05-26T11:42:00Z", "58.15716"));

      final PriceHistory history2 = history1.updateHistory(
        Instant.parse("2022-05-26T11:42:59.381Z"), new BigDecimal("31.27160"));

      assertThat(history2.getEntries())
        .hasSize(1)
        .first()
        .isEqualTo(priceFrame("2022-05-26T11:42:00Z", "31.27160", "58.15716"));

      final PriceHistory history3 = history2.updateHistory(
        Instant.parse("2022-05-26T11:43:07.619Z"), new BigDecimal("34.62701"));
      assertThat(history3.getEntries())
        .hasSize(2)
        .containsExactly(
          priceFrame("2022-05-26T11:42:00Z", "31.27160", "58.15716"),
          priceFrame("2022-05-26T11:43:00Z", "34.62701"));

      final PriceHistory history4 = history3.updateHistory(
        Instant.parse("2022-05-26T11:44:25.007Z"), new BigDecimal("60.70182"));
      assertThat(history4.getEntries())
        .hasSize(2)
        .containsExactly(
          priceFrame("2022-05-26T11:43:00Z", "34.62701"),
          priceFrame("2022-05-26T11:44:00Z", "60.70182"));

      final PriceHistory history5 = history4.updateHistory(
        Instant.parse("2022-05-26T11:44:25.007Z"), new BigDecimal("60.70182"));
      assertThat(history5.getEntries())
        .isEqualTo(history4.getEntries());
    }
  }

  private PriceFrame priceFrame(String time, String price) {
    return PriceFrame.of(context, Instant.parse(time), new BigDecimal(price));
  }

  private PriceFrame priceFrame(String time, String minPrice, String maxPrice) {
    return priceFrame(time, minPrice).updateRange(context, Instant.parse(time), new BigDecimal(maxPrice));
  }
}
