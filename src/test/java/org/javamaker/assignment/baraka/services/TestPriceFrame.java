package org.javamaker.assignment.baraka.services;

import static org.assertj.core.api.Assertions.*;
import static org.junit.jupiter.api.Assertions.*;

import java.math.BigDecimal;
import java.time.Instant;
import java.time.temporal.ChronoUnit;

import org.javamaker.assignment.baraka.JUnitBase;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

@DisplayName("PriceFrame")
class TestPriceFrame extends JUnitBase {
  private PipelineContext context;

  @BeforeEach
  void setUp() {
    context = new PipelineContext(defaultPipelineProperties());
  }

  @DisplayName("of(PipelineContext, Instant, BigDecimal)")
  @Nested
  class TestCreate {
    final Instant now = truncatedNow();

    @DisplayName("null context")
    @Test
    void testNullContext() {
      assertThatNullPointerException()
        .isThrownBy(() -> PriceFrame.of(null, null, null))
        .withMessageContainingAll("context", "must not be null");
    }

    @DisplayName("null epochTime")
    @Test
    void testNullTimestamp() {
      assertThatNullPointerException()
        .isThrownBy(() -> PriceFrame.of(context, null, null))
        .withMessageContainingAll("epochTime", "must not be null");
    }

    @DisplayName("null priceValue")
    @Test
    void testNullPriceValue() {
      assertThatNoException()
        .isThrownBy(() -> PriceFrame.of(context, now, null));

      final PriceFrame range = PriceFrame.of(context, now, null);
      assertNotNull(range);
      assertEquals(now, range.getTimestamp());
      assertNull(range.getMinPrice());
      assertNull(range.getMaxPrice());
    }

    @DisplayName("success path")
    @Test
    void testAllArguments() {
      final PriceFrame range = PriceFrame.of(context, now, BigDecimal.ONE);
      assertEquals(now.truncatedTo(ChronoUnit.MINUTES), range.getTimestamp());
      assertEquals(BigDecimal.ONE, range.getMinPrice());
      assertEquals(BigDecimal.ONE, range.getMaxPrice());
    }
  }

  @DisplayName("updateRange(PipelineContext, Instant, BigDecimal)")
  @Nested
  class TestUpdateRange {
    final Instant now = truncatedNow();

    @DisplayName("null timestamp")
    @Test
    void testNullTimestamp() {
      final PriceFrame range = PriceFrame.of(context, now, null);

      assertThatNullPointerException()
        .isThrownBy(() -> range.updateRange(context, null, null))
        .withMessageContainingAll("epochTime", "must not be null");

      assertThatNoException()
        .isThrownBy(() -> range.updateRange(context, now, null));
    }

    @DisplayName("null priceValue")
    @Test
    void testNullPriceValue() {
      final PriceFrame range = PriceFrame.of(context, now, BigDecimal.ONE);

      assertThatNoException()
        .isThrownBy(() -> range.updateRange(context, now, null));
      assertThat(range.updateRange(context, now, null))
        .isSameAs(range);
    }

    @DisplayName("success path")
    @Test
    void testAllArguments() {
      final PriceFrame initialRange = PriceFrame.of(context, now, null);

      final PriceFrame rangeUpdate1 = initialRange.updateRange(context, now, BigDecimal.ONE);
      assertEquals(BigDecimal.ONE, rangeUpdate1.getMinPrice());
      assertEquals(BigDecimal.ONE, rangeUpdate1.getMaxPrice());

      final PriceFrame rangeUpdate2 = rangeUpdate1.updateRange(context, now, BigDecimal.ONE);
      assertSame(rangeUpdate1, rangeUpdate2);

      final PriceFrame rangeUpdate3 = rangeUpdate2.updateRange(context, now, BigDecimal.TEN);
      assertEquals(BigDecimal.ONE, rangeUpdate3.getMinPrice());
      assertEquals(BigDecimal.TEN, rangeUpdate3.getMaxPrice());

      assertThat(rangeUpdate3.updateRange(context, now, BigDecimal.TEN))
        .isSameAs(rangeUpdate3);
      assertThat(rangeUpdate3.updateRange(context, now.plusSeconds(10L), BigDecimal.TEN))
        .isSameAs(rangeUpdate3);

      final PriceFrame rangeUpdate4 = rangeUpdate3.updateRange(context,
        now.plus(10L, ChronoUnit.SECONDS), BigDecimal.valueOf(20));
      assertEquals(now, rangeUpdate4.getTimestamp());
      assertEquals(BigDecimal.ONE, rangeUpdate4.getMinPrice());
      assertEquals(BigDecimal.valueOf(20), rangeUpdate4.getMaxPrice());

      final Instant minuteAfterNow = now.plus(1L, ChronoUnit.MINUTES);
      final PriceFrame rangeUpdate5 = rangeUpdate4.updateRange(context, minuteAfterNow, BigDecimal.TEN);
      assertEquals(minuteAfterNow, rangeUpdate5.getTimestamp());
      assertEquals(BigDecimal.TEN, rangeUpdate5.getMinPrice()); // new interval, minimum is reset
      assertEquals(BigDecimal.TEN, rangeUpdate5.getMaxPrice());
    }
  }

  @DisplayName("isAfter(PriceFrame)")
  @Nested
  class TestIsAfter {
    final Instant now = truncatedNow();

    @DisplayName("null otherEntry")
    @Test
    void testNullOtherEntry() {
      final PriceFrame range = PriceFrame.of(context, now, null);

      assertThatNullPointerException()
        .isThrownBy(() -> range.isAfter(null))
        .withMessageContainingAll("otherEntry", "must not be null");
    }

    @DisplayName("success path")
    @Test
    void testAllArguments() {
      final PriceFrame initialRange = PriceFrame.of(context, now, null);

      assertThatNullPointerException()
        .isThrownBy(() -> initialRange.isAfter(null))
        .withMessageContainingAll("otherEntry", "must not be null");

      final PriceFrame rangeUpdate1 = initialRange.updateRange(context, now, BigDecimal.ONE);
      assertFalse(initialRange.isAfter(rangeUpdate1));
      assertFalse(rangeUpdate1.isAfter(initialRange));

      final Instant minuteAfterNow = now.plus(1L, ChronoUnit.MINUTES);
      final PriceFrame rangeUpdate2 = rangeUpdate1.updateRange(context, minuteAfterNow, BigDecimal.TEN);
      assertFalse(initialRange.isAfter(rangeUpdate2));
      assertTrue(rangeUpdate2.isAfter(initialRange));
    }
  }

  private static Instant truncatedNow() {
    return Instant.now().truncatedTo(ChronoUnit.MINUTES);
  }
}
