package org.javamaker.assignment.baraka.services;

import static org.assertj.core.api.Assertions.*;
import static org.junit.jupiter.api.Assertions.*;

import java.math.BigDecimal;
import java.time.Instant;

import org.javamaker.assignment.baraka.JUnitBase;
import org.javamaker.assignment.baraka.model.MarketMessage;
import org.javamaker.assignment.baraka.model.StockPrice;
import org.javamaker.assignment.baraka.util.ObjectMappers;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.springframework.core.convert.ConversionFailedException;

import com.fasterxml.jackson.databind.ObjectMapper;

@DisplayName("EventDecoder")
class TestEventDecoder extends JUnitBase {

  @DisplayName("<init>(Class<T>, ObjectMapper)")
  @Nested
  class TestCreate {
    @DisplayName("null eventType")
    @Test
    void testNullEventType() {
      assertThatNullPointerException()
        .isThrownBy(() -> new EventDecoder<>(null, new ObjectMapper()))
        .withMessageContainingAll("eventType", "must not be null");
    }

    @DisplayName("null objectMapper")
    @Test
    void testNullObjectMapper() {
      assertThatNullPointerException()
        .isThrownBy(() -> new EventDecoder<>(String.class, null))
        .withMessageContainingAll("objectMapper", "must not be null");
    }

    @DisplayName("success path")
    @Test
    void testAllArguments() {
      assertThatNoException().isThrownBy(() -> new EventDecoder<>(String.class, new ObjectMapper()));
    }
  }

  @DisplayName("apply(String)")
  @Nested
  class TestApply {
    @DisplayName("exception")
    @Test
    void testException() {
      final EventDecoder<MarketMessage> decoder = new EventDecoder<>(
        MarketMessage.class, ObjectMappers.webSocketObjectMapper());

      final String illegalJson = "{\"data:[],\"type\":\"trade\"}";

      assertThatThrownBy(() -> decoder.apply(illegalJson))
        .isInstanceOf(ConversionFailedException.class)
        .hasMessageContainingAll(illegalJson, MarketMessage.class.getName());
    }

    @DisplayName("success")
    @Test
    void testSuccessPath() {
      final EventDecoder<MarketMessage> decoder = new EventDecoder<>(
        MarketMessage.class, ObjectMappers.webSocketObjectMapper());

      final MarketMessage message = decoder.apply("{\"data\":[{\"p\":93.85099174403666,"
        + "\"s\":\"AAPL\",\"t\":1653577493139,\"v\":10}],\"type\":\"trade\"}");

      assertEquals("trade", message.getType());

      final StockPrice expectedPrice = new StockPrice();
      expectedPrice.setStock("AAPL");
      expectedPrice.setTime(Instant.ofEpochMilli(1653577493139L));
      expectedPrice.setPrice(new BigDecimal("93.85099174403666"));
      expectedPrice.setVolume(10);

      assertThat(message.getData()).containsExactly(expectedPrice);
    }
  }
}
