package org.javamaker.assignment.baraka;

import org.apache.commons.lang3.exception.ExceptionUtils;
import org.junit.jupiter.api.extension.ExtensionContext;
import org.junit.jupiter.api.extension.TestExecutionExceptionHandler;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class ExceptionLogger implements TestExecutionExceptionHandler {
  @Override
  public void handleTestExecutionException(ExtensionContext context, Throwable throwable) throws Throwable {
    if (throwable instanceof AssertionError) {
      log.error(ExceptionUtils.getMessage(throwable));
    } else {
      log.error("", throwable);
    }
    throw throwable;
  }
}
